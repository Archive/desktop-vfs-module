/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/* control-center-method.c

   Copyright (C) 1998 Redhat Software Inc.
   Copyright (C) 2001 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Authors: Jonathan Blandford <jrb@redhat.com>
	    John Harper <jsh@eazel.com> 
*/

/* URI scheme for accessing the tree of control center desktop files by their
   actual names. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "control-center-method.h"

#include <glib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include <libgnomevfs/gnome-vfs-mime.h>

#include <libgnomevfs/gnome-vfs-module.h>
#include <libgnomevfs/gnome-vfs-method.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfs/gnome-vfs-module-shared.h>

#undef _
#include <libgnome/libgnome.h>
#include <libgnome/gnome-dentry.h>

static GNode *root_node;

#ifdef PATH_MAX
#define	GET_PATH_MAX()	PATH_MAX
#else
static int
GET_PATH_MAX (void)
{
	static unsigned int value;

	/* This code is copied from GNU make.  It returns the maximum
	   path length by using `pathconf'.  */

	if (value == 0) {
		long int x = pathconf(G_DIR_SEPARATOR_S, _PC_PATH_MAX);

		if (x > 0)
			value = x;
		else
			return MAXPATHLEN;
	}

	return value;
}
#endif


/* code borrowed from control center to read the tree of desktop entries */ 

static gint
compare_last_dir (gchar *first, gchar *second)
{
        gboolean retval;
        gchar *temp1;
        gchar *temp2;

        temp1 = g_strdup (first);
        temp2 = g_strdup (second);
        (rindex(temp1, '/'))[0] = 0;
        (rindex(temp2, '/'))[0] = 0;

        retval = strcmp (rindex (temp1, '/'), rindex (temp2, '/')) == 0;

        g_free (temp1);
        g_free (temp2);

        return retval;
}

static gboolean
compare_nodes (GnomeDesktopEntry *data1, GnomeDesktopEntry *data2)
{
        g_return_val_if_fail (data1, FALSE);
        g_return_val_if_fail (data2, FALSE);
        g_return_val_if_fail (data1->type, FALSE);
        g_return_val_if_fail (data2->type, FALSE);

        if (strcmp (data1->type, "Directory") == 0
	    && strcmp (data2->type, "Directory") == 0) {
                return compare_last_dir (data1->location,
                                         data2->location);
	} else {
                return (strcmp (rindex (data1->location,'/'),
				rindex (data2->location,'/')) == 0);
	}
}

/* This function is used to generate a node starting at a directory.
 * It doesn't do all that complex error checking -- if something
 * happens, it just returns null and skips the directory.
 *
 * It will try to use node1's data over node2's if possible, and will
 * write into node1's field.  It should handle all memory, so there is
 * no need to free stuff from node2 after the merger.
 */
static void
merge_nodes (GNode *node1, GNode *node2)
{
        GNode *child1, *child2;
	GnomeDesktopEntry *entry1;
	GnomeDesktopEntry *entry2;

	if ((node1 == NULL) || (node2 == NULL)) {
                return;
	}

        /* first we merge data */
        if (node1->data == NULL) {
                node1->data = node2->data;
        } else if (node2->data != NULL) {
                entry1 = (GnomeDesktopEntry *) node1->data;
                entry2 = (GnomeDesktopEntry *) node2->data;

                if (entry1->name == NULL && entry2->name != NULL)
                        entry1->name = g_strdup (entry2->name);

                gnome_desktop_entry_free (node2->data);
                node2->data = NULL;
        }

        /* now we want to find subdirs to merge */
        /* it's not incredibly effecient, but it works... */
        for (child1 = node1->children; child1; child1 = child1->next) {
                for (child2 = node2->children; child2; child2 = child2->next) {
			if (child1->data == NULL || child2->data == NULL) {
                                continue;
			}
                        if (compare_nodes (child1->data, child2->data)) {
				if (child2->prev == NULL) {
                                        child2->parent->children = child2->next;
				} else {
                                        child2->prev->next = child2->next;
				}
                                merge_nodes (child1, child2);
                        }
                }
        }

        if (node2->children) {
		for (child2 = node2->children; child2->next; child2 = child2->next) {
                        child2->parent = node1;
		}
                child2->next = node1->children;
                child2->next->prev = child2;
                node1->children = node2->children;
                node2->children = NULL;
        }
}

static GNode *
read_directory_entries (gchar *directory)
{
        DIR *parent_dir;
        struct dirent *child_dir;
        struct stat filedata;
        GNode *retval;
	GString *name;
	char *test;
	GNode *next_dir, *new_node;
	GnomeDesktopEntry *entry;

        parent_dir = opendir (directory);
	if (parent_dir == NULL) {
                return NULL;
	}

	retval = g_node_new (NULL);
        while ((child_dir = readdir (parent_dir)) != NULL) {
                if (child_dir->d_name[0] != '.') {

                        /* we check to see if it is interesting. */
                        name = g_string_new (directory);
                        g_string_append (name, "/");
                        g_string_append (name, child_dir->d_name);

                        if (stat (name->str, &filedata) != -1) {
                                if (S_ISDIR (filedata.st_mode)) {
                                        /* it might be interesting... */
                                        next_dir = read_directory_entries (name->str);
					if (next_dir != NULL) {
                                                /* it is interesting!!! */
                                                g_node_prepend (retval, next_dir);
					}
                                }
                                test = rindex(child_dir->d_name, '.');
                                if (test != NULL
				    && strcmp (".desktop", test) == 0) {
                                        /* it's a .desktop file
					 * -- it's interesting for sure!
					 */
                                        new_node = g_node_new (gnome_desktop_entry_load (name->str));
                                        g_node_prepend (retval, new_node);
                                }
                        }
                        g_string_free (name, TRUE);
                }
                else if (!strcmp (child_dir->d_name, ".directory")) {
                        name = g_string_new (directory);
                        g_string_append (name, "/.directory");
			entry = gnome_desktop_entry_load (name->str);
                        g_string_free (name, TRUE);

			g_free (entry->location);
			entry->location = g_strdup (directory);

                        retval->data = entry;
                }
        }
        
        closedir (parent_dir);

        if (retval->data == NULL) {
                /* No `.directory' file.  Create it from the name
		 * of the directory.
		 */
                entry = g_new0 (GnomeDesktopEntry, 1);
                entry->type          = g_strdup ("Directory");   
                entry->location      = g_strdup (directory);

                retval->data = entry;
        }

        if (retval->children == NULL) {
                if (retval->data) {
                        gnome_desktop_entry_free (retval->data);
                        retval->data = NULL;
                }
                return NULL;
        }

        return retval;
}

static gboolean
add_directories_from_GNOME_PATH (GList **list)
{
        char *gnome_path;
        char **split_gnome_path;
        char **p;
	char *directory;

        gnome_path = getenv ("GNOME_PATH");
	if (gnome_path == NULL) {
                return FALSE;
	}

        split_gnome_path = g_strsplit (gnome_path, ":", -1);

        for (p = split_gnome_path; *p != NULL; p++) {
                directory = g_concat_dir_and_file (*p, "share/control-center");
                *list = g_list_prepend (*list, directory);
        }

        g_strfreev (split_gnome_path);

        return TRUE;
}

static GList *
get_directory_list (void)
{
        GList *list;

        list = NULL;

        list = g_list_prepend (list, gnome_unconditional_datadir_file ("control-center"));
        list = g_list_prepend (list, gnome_util_home_file ("control-center"));

        add_directories_from_GNOME_PATH (&list);

        return list;
}

static void
free_directory_list (GList *list)
{
        GList *p;

	for (p = list; p != NULL; p = p->next) {
                g_free (p->data);
	}

        g_list_free (list);
}

static GNode *
read_all_directory_entries (GList *list)
{
        GNode *root_node, *new_node;
        GList *p;

        root_node = NULL;

        for (p = list; p != NULL; p = p->next) {
                new_node = read_directory_entries ((char *) p->data);

		if (new_node == NULL) {
                        continue;
		}

		if (root_node == NULL) {
                        root_node = new_node;
		} else {
                        merge_nodes (root_node, new_node);
		}
        }

        return root_node;
}


/* misc utilities */

static gchar *
get_path_from_uri (GnomeVFSURI *uri)
{
	gchar *path, *longer_path;

	path = gnome_vfs_unescape_string (uri->text, G_DIR_SEPARATOR_S);
	if (path == NULL) {
		return NULL;
	}

	/* This is to make sure the path starts with a "/", so that at
	 * least we get a predictable behavior when the
	 * leading "/" is not present.
	 */
	if (path[0] == G_DIR_SEPARATOR) {
		return path;
	}
	longer_path = g_strconcat (G_DIR_SEPARATOR_S, path, NULL);
	g_free (path);
	return longer_path;
}

static gboolean
find_callback (GNode *node, gpointer data)
{
	gpointer *d = data;
	GnomeDesktopEntry *de;

	if (node == d[0]) {
		return FALSE;
	} else {
		de = node->data;
		if (strcmp (d[1], de->name) == 0) {
			/* found it */
			d[2] = node;
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

static GNode *
find_node (char *path)
{
        char **split_path, **p;
	GNode *node;
	gpointer closure[3];

	while (*path != 0 && *path == '/')
	    path++;

	split_path = g_strsplit (path, "/", -1);
	node = root_node;
	for (p = split_path; node != NULL && *p != NULL; p++) {
		closure[0] = node;
		closure[1] = *p;
		closure[2] = NULL;
		g_node_traverse (node, G_LEVEL_ORDER, G_TRAVERSE_ALL,
				 2, find_callback, closure);
		node = closure[2];
	}
	g_strfreev (split_path);
	return  node;
}


/* gnome-vfs methods */

typedef struct {
	GnomeVFSURI *uri;
	GNode *node;
	char *data;
	size_t data_size;
	int read_ptr;
} FileHandle;

static FileHandle *
file_handle_new (GnomeVFSURI *uri, GNode *node)
{
	GnomeDesktopEntry *de;
	FileHandle *result;
	char *exec_string;

	de = node->data;
	result = g_new (FileHandle, 1);

	result->uri = gnome_vfs_uri_ref (uri);
	result->node = node;
	result->data = g_malloc (1024);

	if (de->exec != NULL) {
		exec_string = g_strjoinv (" ", de->exec);
	} else {
		exec_string = NULL;
	}
	g_snprintf (result->data, 1024, "[Desktop Entry]\nName=%s\nComment=%s\nExec=%s\nTryExec=%s\nIcon=%s\nType=%s\n",
		    de->name,
		    de->comment ? de->comment : "",
		    exec_string ? exec_string : "",
		    de->tryexec ? de->tryexec : "",
		    de->icon ? de->icon : "",
		    de->type ? de->type : "");
	g_free (exec_string);

	result->data_size = strlen (result->data);
	result->read_ptr = 0;

	return result;
}

static void
file_handle_destroy (FileHandle *handle)
{
	gnome_vfs_uri_unref (handle->uri);
	g_free (handle->data);
	g_free (handle);
}

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI *uri,
	 GnomeVFSOpenMode mode,
	 GnomeVFSContext *context)
{
	FileHandle *file_handle;
	GNode *node;
	char *file_name;

	_GNOME_VFS_METHOD_PARAM_CHECK (method_handle != NULL);
	_GNOME_VFS_METHOD_PARAM_CHECK (uri != NULL);

	if (mode == GNOME_VFS_OPEN_READ) {
		file_name = get_path_from_uri (uri);
		if (file_name == NULL) {
			node = root_node;
		} else {
			node = find_node (file_name);
		}
		if (node == NULL) {
			return GNOME_VFS_ERROR_NOT_FOUND;
		}


		file_handle = file_handle_new (uri, node);
	
		*method_handle = (GnomeVFSMethodHandle *) file_handle;

		return GNOME_VFS_OK;

	} else if (mode == GNOME_VFS_OPEN_WRITE) {
		return GNOME_VFS_ERROR_READ_ONLY;
	} else {
		return GNOME_VFS_ERROR_INVALID_OPEN_MODE;
	}
}

static GnomeVFSResult
do_close (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext *context)
{
	FileHandle *file_handle;

	g_return_val_if_fail (method_handle != NULL, GNOME_VFS_ERROR_INTERNAL);

	file_handle = (FileHandle *) method_handle;

	file_handle_destroy (file_handle);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer buffer,
	 GnomeVFSFileSize num_bytes,
	 GnomeVFSFileSize *bytes_read,
	 GnomeVFSContext *context)
{
	FileHandle *file_handle;
	gint read_val;

	g_return_val_if_fail (method_handle != NULL, GNOME_VFS_ERROR_INTERNAL);

	file_handle = (FileHandle *) method_handle;

	read_val = MIN (num_bytes, file_handle->data_size - file_handle->read_ptr);
	if (read_val > 0) {
		memcpy (buffer, file_handle->data
			+ file_handle->read_ptr, read_val);
		file_handle->read_ptr += read_val;
	}

	*bytes_read = read_val;

	/* Getting 0 from read() means EOF! */
	if (read_val == 0) {
		return GNOME_VFS_ERROR_EOF;
	} else {
		return GNOME_VFS_OK;
	}
}

typedef struct {
	GnomeVFSURI *uri;
	GNode *node;
	int index;
	GnomeVFSFileInfoOptions options;

	gchar *name_buffer;
	gchar *name_ptr;

	const GnomeVFSDirectoryFilter *filter;
} DirectoryHandle;

static DirectoryHandle *
directory_handle_new (GnomeVFSURI *uri,
		      GNode *node,
		      GnomeVFSFileInfoOptions options,
		      const GnomeVFSDirectoryFilter *filter)
{
	DirectoryHandle *result;
	gchar *full_name;
	guint full_name_len;

	result = g_new (DirectoryHandle, 1);

	result->uri = gnome_vfs_uri_ref (uri);
	result->node = node;
	result->index = 0;

	full_name = get_path_from_uri (uri);
	if (full_name != NULL) {
		full_name_len = strlen (full_name);
	} else {
		full_name_len = 0;
	}

	result->name_buffer = g_malloc (full_name_len + GET_PATH_MAX () + 2);
	memcpy (result->name_buffer, full_name, full_name_len);
	
	if (full_name_len > 0 && full_name[full_name_len - 1] != '/')
		result->name_buffer[full_name_len++] = '/';

	result->name_ptr = result->name_buffer + full_name_len;

	g_free (full_name);

	result->options = options;
	result->filter = filter;

	return result;
}

static void
directory_handle_destroy (DirectoryHandle *directory_handle)
{
	gnome_vfs_uri_unref (directory_handle->uri);
	g_free (directory_handle->name_buffer);
	g_free (directory_handle);
}

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   const GnomeVFSDirectoryFilter *filter,
		   GnomeVFSContext *context)
{
	gchar *directory_name;
	GNode *node;

	directory_name = get_path_from_uri (uri);
	if (directory_name == NULL) {
		node = root_node;
	} else {
		node = find_node (directory_name);
	}
	g_free (directory_name);
	if (node == NULL) {
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	*method_handle
		= (GnomeVFSMethodHandle *) directory_handle_new (uri, node,
								 options,
								 filter);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	DirectoryHandle *directory_handle;

	directory_handle = (DirectoryHandle *) method_handle;

	directory_handle_destroy (directory_handle);

	return GNOME_VFS_OK;
}

static gchar *
read_link (const gchar *full_name)
{
	gchar *buffer;
	guint size;

	size = 256;
	buffer = g_malloc (size);
          
	while (1) {
		int read_size;

                read_size = readlink (full_name, buffer, size);
		if (read_size < 0) {
			return NULL;
		}
                if (read_size < size) {
			buffer[read_size] = 0;
			return buffer;
		}
                size *= 2;
		buffer = g_realloc (buffer, size);
	}
}

static GnomeVFSResult
get_stat_info (GnomeVFSFileInfo *file_info,
	       const gchar *full_name,
	       GnomeVFSFileInfoOptions options,
	       struct stat *statptr)
{
	struct stat statbuf;
	gboolean followed_symlink;

	if (statptr == NULL) {
		statptr = &statbuf;
	}

	if (lstat (full_name, statptr) != 0) {
		return gnome_vfs_result_from_errno ();
	}

	if ((options & GNOME_VFS_FILE_INFO_FOLLOW_LINKS) && S_ISLNK (statptr->st_mode)) {
		if (stat (full_name, statptr) != 0) {
			if (errno == ENOENT) {
				/* its a broken symlink, revert to the lstat */
				lstat (full_name, statptr);
				followed_symlink = TRUE;
			} else {
				/* go straight to jail, do not pass GO, do not collect $200 */
				return gnome_vfs_result_from_errno();
			}
		} else {
			followed_symlink = TRUE;
		}		
	} else {
		/* follow links wasn't on, or this wasn't a symlink */
		followed_symlink = FALSE;
	}

	gnome_vfs_stat_to_file_info (file_info, statptr);
	file_info->permissions &= ~(S_IWUSR | S_IWGRP | S_IWOTH);
	GNOME_VFS_FILE_INFO_SET_SYMLINK (file_info, followed_symlink);

	GNOME_VFS_FILE_INFO_SET_LOCAL (file_info, TRUE);

	if (S_ISLNK (statptr->st_mode)) {
		file_info->type = GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK;
		file_info->symlink_name = read_link (full_name);
		if (file_info->symlink_name == NULL) {
			return gnome_vfs_result_from_errno ();
		}
		file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME;
	}

	return GNOME_VFS_OK;
}

static void
get_mime_type (GnomeVFSFileInfo *info,
	       GnomeDesktopEntry *de,
	       GnomeVFSFileInfoOptions options,
	       struct stat *stat_buffer)
{
	const char *type = gnome_vfs_mime_type_from_mode (stat_buffer->st_mode);
	if (type == NULL) {
		if (strcmp (de->type, "Application") == 0) {
			type = "application/x-gnome-control-center-applet";
		} else {
			type = "application/x-gnome-app-info";
		}
	}
	info->mime_type = g_strdup (type);
	info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
}

static inline GnomeVFSResult
read_directory (DirectoryHandle *handle,
		GnomeVFSFileInfo *info,
		gboolean *skip,
		GnomeVFSContext *context)
{
	const GnomeVFSDirectoryFilter *filter;
	GnomeVFSDirectoryFilterNeeds filter_needs;
	GNode *result;
	GnomeDesktopEntry *de;
	struct stat statbuf;
	gchar *full_name;
	gboolean filter_called;

	/* This makes sure we don't try to filter the file more than
           once.  */
	filter_called = FALSE;

	filter = handle->filter;
	if (filter != NULL) {
		filter_needs = gnome_vfs_directory_filter_get_needs (filter);
	} else {
		filter_needs = GNOME_VFS_DIRECTORY_FILTER_NEEDS_NOTHING;
	}

	result = g_node_nth_child (handle->node, handle->index);
	if (result == NULL) {
		return GNOME_VFS_ERROR_EOF;
	} else {
		handle->index++;
	}

	de = result->data;
	info->name = g_strdup (de->name);

	if (filter != NULL
	    && !filter_called
	    && (filter_needs
		  & (GNOME_VFS_DIRECTORY_FILTER_NEEDS_TYPE
		     | GNOME_VFS_DIRECTORY_FILTER_NEEDS_STAT
		     | GNOME_VFS_DIRECTORY_FILTER_NEEDS_MIMETYPE)) == 0){
		if (!gnome_vfs_directory_filter_apply (filter, info)) {
			*skip = TRUE;
			return GNOME_VFS_OK;
		}

		filter_called = TRUE;
	}

	strcpy (handle->name_ptr, de->name);
	full_name = handle->name_buffer;

	if (get_stat_info (info, de->location, handle->options, &statbuf) != GNOME_VFS_OK) {
		return GNOME_VFS_ERROR_INTERNAL;
	}

	if (filter != NULL
	    && !filter_called
	    && (filter_needs
		  & GNOME_VFS_DIRECTORY_FILTER_NEEDS_MIMETYPE) == 0) {
		if (! gnome_vfs_directory_filter_apply (filter, info)) {
			*skip = TRUE;
			return GNOME_VFS_OK;
		}
		filter_called = TRUE;
	}

	if (handle->options & GNOME_VFS_FILE_INFO_GET_MIME_TYPE) {
		get_mime_type (info, de, handle->options, &statbuf);
	}
	if (filter != NULL
	    && !filter_called) {
		if (!gnome_vfs_directory_filter_apply (filter, info)) {
			*skip = TRUE;
			return GNOME_VFS_OK;
		}
		filter_called = TRUE;
	}

	if (filter != NULL && !filter_called) {
		if (!gnome_vfs_directory_filter_apply (filter, info)) {
			*skip = TRUE;
			return GNOME_VFS_OK;
		}
		filter_called = TRUE;
	}

	*skip = FALSE;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	GnomeVFSResult result;
	gboolean skip;

	do {
		result = read_directory ((DirectoryHandle *) method_handle,
					 file_info, &skip, context);
		if (result != GNOME_VFS_OK)
			break;
		if (skip)
			gnome_vfs_file_info_clear (file_info);
	} while (skip);

	return result;
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)
{
	GnomeVFSResult result;
	gchar *full_name;
	struct stat statbuf;
	GNode *node;
	GnomeDesktopEntry *de;

	full_name = get_path_from_uri (uri);
	if (full_name == NULL) {
		node = root_node;
	} else {
		node = find_node (full_name);
	}

	file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE;

	if (node == NULL) {
		g_free (full_name);
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	de = node->data;

	file_info->name = g_strdup (de->name);

	result = get_stat_info (file_info, de->location, options, &statbuf);
	if (result != GNOME_VFS_OK) {
		g_free (full_name);
		return result;
	}

	if (options & GNOME_VFS_FILE_INFO_GET_MIME_TYPE) {
		get_mime_type (file_info, de, options, &statbuf);
	}

	g_free (full_name);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_check_same_fs (GnomeVFSMethod *method, GnomeVFSURI *a, GnomeVFSURI *b,
		  gboolean *same_fs_return, GnomeVFSContext *context)
{
	*same_fs_return = TRUE;

	return GNOME_VFS_OK;
}

static gboolean       
do_is_local (GnomeVFSMethod *method, const GnomeVFSURI *uri)
{
	return TRUE;
}


/* gnome-vfs bureaucracy */

static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	NULL,		/* do_create */
	do_close,
	do_read,	/* do_read */
	NULL,		/* do_write */
	NULL,		/* seek */
	NULL,		/* tell */
	NULL,		/* truncate */
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	NULL,
	do_is_local,
	NULL,		/* make directory */
	NULL,		/* remove directory */
	NULL,		/* rename */
	NULL,		/* unlink */
	do_check_same_fs,
	NULL,		/* do_set_file_info */
	NULL,		/* do_truncate */
	NULL,		/* do_find_directory */
	NULL		/* do_create_symbolic_link */
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, 
		 const char *args)
{
	GList *directory_list;

	gnomelib_init ("gnome-vfs-control-center-method", "0.0");

	directory_list = get_directory_list ();
	root_node = read_all_directory_entries (directory_list);
	free_directory_list (directory_list);

	return &method;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
}
