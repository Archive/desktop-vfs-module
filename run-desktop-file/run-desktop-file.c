/* run-capplet.c -- 

   Copyright (C) 2001 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>   */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>

static void
vfs_perror_and_exit (const char *uri, GnomeVFSResult result)
{
	const char *error;

	error = gnome_vfs_result_to_string (result);
	if (error != NULL) {
		g_error ("%s: %s", error, uri);
	} else {
		g_error ("Unable to read %s", uri);
	}

	exit (1);
}

static char *
read_exec_string (const char *file_uri)
{
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	GnomeVFSFileInfo info;
	char *buffer;
	GnomeVFSFileSize bytes_read;
	char *ptr, *next;
	char *exec_string;

	exec_string = NULL;

	result = gnome_vfs_get_file_info (file_uri, &info,
					  GNOME_VFS_FILE_INFO_DEFAULT);
	if (result != GNOME_VFS_OK) {
		vfs_perror_and_exit (file_uri, result);
	}

	result = gnome_vfs_open (&handle, file_uri, GNOME_VFS_OPEN_READ);
	if (result != GNOME_VFS_OK) {
		vfs_perror_and_exit (file_uri, result);
	}

	buffer = g_malloc (info.size + 1);
	result = gnome_vfs_read (handle, buffer, info.size, &bytes_read);
	if (result == GNOME_VFS_OK) {
		buffer[bytes_read] = 0;
		ptr = buffer;
		while (ptr != NULL) {
			next = strchr (ptr, '\n');
			if (strncmp ("Exec=", ptr, 5) == 0) {
				if (next != NULL) {
					*next = 0;
				}
				if (ptr[5] != 0) {
					exec_string = g_strdup (ptr + 5);
				}
				break;
			}
			ptr = next ? next + 1 : NULL;
		}
	} else {
		vfs_perror_and_exit (file_uri, result);
	}

	g_free (buffer);
	gnome_vfs_close (handle);

	return exec_string;
}

int
main (int argc, char **argv)
{
	GSList *to_exec, *ptr;
	char *exec_string;
	char *buf;
	int stat;
	int i;

	if (argc < 2) {
		fprintf (stderr, "usage: run-desktop-file DESKTOP-FILE-URI");
		return 1;
	}

	gnome_vfs_init ();

	to_exec = NULL;
	for (i = 1; i < argc; i++) {
		char *tem = read_exec_string (argv[i]);
		if (tem != NULL) {
			to_exec = g_slist_append (to_exec, tem);
		}
	}

	gnome_vfs_shutdown ();

	for (ptr = to_exec; ptr != NULL; ptr = g_slist_next (ptr)) {
		buf = g_malloc (strlen (ptr->data) + 32);
		sprintf (buf, "%s </dev/null &", ptr->data);
		stat = system (buf);
		if (stat != 0) {
			g_error ("Unable to successully run `%s'", buf);
			exit (1);
		}
	}

	return 0;
}
